require 'rails_helper'

describe "athlete" do
  let(:athletes) { Athlete.find("jess brown", 1) }

  describe "from athlete search" do
    it 'returns the data' do
      expect(athletes.count).to eq 2
    end

    it 'gets the data into a athlete object' do
      expect(athletes.first.name).to eq "Jess Brown"
      expect(athletes.first.id).to eq "506709"
      expect(athletes.first.affiliate).to eq "CrossFit Loganville"
    end
  end

  describe "from search result" do
    let(:athlete) { Result.new(506709).athlete }

    it 'gets the name' do
      expect(athlete.name).to eq "Jess Brown"
      expect(athlete.division_id).to eq "18"
    end
  end
end
