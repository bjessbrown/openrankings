require 'rails_helper'

describe "Results" do
  subject { Result.new(506709, 18, "GA") }

  describe "world rank" do
    it 'returns the world rank' do
      expect(subject.world.rank.to_s).to match /\d{4}/
      expect(subject.athlete.region_id).to eq 15.to_s
      expect(subject.athlete.division_id).to eq 18.to_s
    end
  end

  describe "region rank" do
    it 'returns the region rank' do
      expect(subject.region.rank.to_s).to match /\d{3}/
    end
  end

  describe "state rank" do
    it 'returns the state rank' do
      expect(subject.state.rank.to_s).to match /\d{2}/
    end
  end

  describe "affiliate rank" do
    it 'returns the affiliate rank' do
      expect(subject.affiliate.rank.to_s).to eq "2"
    end
  end

  describe "#total_athletes" do
    it 'returns ' do
      expect(subject.state.total_pages).to eq 15
      expect(subject.state.total_athletes).to eq 720
    end
  end

end
