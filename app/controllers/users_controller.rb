class UsersController < ApplicationController
  def index
    if params[:search].present?
      @users = Athlete.find(params[:search][:username], params[:search][:division])
    end
  end

  def show
    @user = params[:id]
    if params[:state].present?
      fittest = 3
      fittest1 = params[:state]
    else
      fittest = params[:fittest]
      fittest1 = params[:fittest1]
    end
    if(@user.present? && params[:division].present? && fittest.present? && fittest1.present?)
      @result = Result.new(@user, params[:division], fittest, fittest1) unless @user == "super"
    else
      redirect_to(root_path, notice: "Please fill in all the fields")
    end
  end
end
