// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require html2canvas
// require foundation
// require turbolinks
//= require_tree .

//$(function(){ $(document).foundation(); });

//$(document).pjax('form button', '.user-results')

$(document).on('change', 'select#fittest', function(event) {
  sel = $(this)
  sel2 = $("select#fittest1")
  val = sel.val()
  sel2.html('')
  opt = $("optgroup." + val).html()
  sel2.html(opt)
})

$(document).on('submit', 'form', function(event) {
	data = $(this).serialize();
	url = $(this).attr("action");
  if(!validateForm(this)) {
    return false;
  }
	$.get( url + "?" + data, function(data){
		html = $(data).find(".user-results")
		$(".user-results").html(html)
	});
	return false;
})

function validateForm(form) {
  name = $("#search_username").val()
  division = $("#search_division").val()
  fittest = $("#fittest").val()
  fittest1 = $("#fittest1").val()
  if(!name || !division || !fittest || !fittest1) {
    alert("Please fill in all the fields.")
    return false;
  } else {
    return true;
  }
}

$(document).ready(function(){

  $("body").on("click", "a.take-screenshot", function(){
    $("tfoot.results-link").show()
    el = $("[data-screenshot]")
    html2canvas(el, {
      onrendered: function(canvas) {
        var image = new Image();
        image.src = canvas.toDataURL("image/png", 1);
        $("p.screenshot").html(image)
        $("p.screenshot-instructions").show()
      }
    });
    return false;
  })
  $("body").on("click", "a.close", function(){
    tr = $(this).parents("tr");
    tr.remove()
    return false;
  })
  $("body").on("click", ".compare a", function(){
    $(".msg").html("Adding......")
    link = $(this);
    href = link.attr("href");
    $.get(href, function(data){
      html = $(data).find("tbody tr")
      $("tbody").append(html)
      $(".msg").html('')
      addAreaToRow(html);
    })
    return false;
  })

  results = $("table tbody.results-table tr")
  if(results.length > 0) {
    addArea();
  }
})

function addArea() {
  $("tbody.results-table tr").each(function() {
    addAreaToRow(this);
  })
}

function addAreaToRow(row) {
    link = $(row).find("td:first a").attr("href")
    split = link.split("&")
    fittest = split.pop().split("=").pop()
    fittest1 = split.pop().split("=").pop()
    cell = $(row).find("td").eq(3)
    fittest1 = $('optgroup.'+ fittest +' option[value="'+fittest1+'"]').text()
    fittest = $('#fittest option[value="'+fittest+'"]').text()
    cell.append("<br><small>" + fittest + " > " + fittest1 + "</small>")
}
