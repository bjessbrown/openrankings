Divisions = {
  "2"=>"Individual Women",
  "1"=>"Individual Men",
  "11"=>"Team",
  "15"=>"Teenage Girls (14-15)",
  "14"=>"Teenage Boys (14-15)",
  "17"=>"Teenage Girls (16-17)",
  "16"=>"Teenage Boys (16-17)",
  "19"=>"Masters Women (35-39)",
  "18"=>"Masters Men (35-39)",
  "13"=>"Masters Women (40-44)",
  "12"=>"Masters Men (40-44)",
  "4"=>"Masters Women (45-49)",
  "3"=>"Masters Men (45-49)",
  "6"=>"Masters Women (50-54)",
  "5"=>"Masters Men (50-54)",
  "8"=>"Masters Women (55-59)",
  "7"=>"Masters Men (55-59)",
  "10"=>"Masters Women (60+)",
  "9"=>"Masters Men (60+)"
}
