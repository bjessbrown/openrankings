class Result
  attr_reader :user_id, :fittest, :fittest1
  attr_accessor :region_id, :division_id, :affiliate_id

  def initialize(user_id, division, fittest, fittest1, json = nil)
    if user_id.is_a?(Athlete)
      @athlete = user_id
      @user_id = athlete.id
      @base_division = division
      @fittest = fittest
      @fittest1 = fittest1
      @json = json
    else
      @user_id = user_id
      @orig_division = division
      @base_division = division
			@fittest = fittest
      @fittest1 = fittest1
    end
  end

  def response
    p url
    @response ||= HTTP.get(url)
  end

  def base_url
    "https://games.crossfit.com/competitions/api/v1/competitions/open/2017/leaderboards?"
  end

  def last_url(page)
    "#{base_url}competition=1&year=2017&division=#{division}&scaled=0&sort=0&fittest=#{fittest}&fittest1=#{fittest1}&occupation=0&page=#{page}"
  end

  def url
    "#{base_url}competition=1&year=2017&division=#{division}&scaled=0&sort=0&fittest=#{fittest}&fittest1=#{fittest1}&occupation=0&athlete=#{user_id}&page=#{page}"
  end

  def page
    1
  end

  def athlete
    @athlete ||= Athlete.new(athlete_info)
  end

  def division
    @base_division
  end

  def division_name
    Divisions[division]
  end

  def json
    @json ||= JSON.parse(response.to_s)
  end

  def rank
    athlete_info["overallrank"].to_i
  end

  def athlete_info
    @athlete_info ||= json["athletes"].find{|a| a["highlight"] == 1 }
  end

  def state
    @state_rank ||= Result::State.new(athlete, division, fittest, fittest1, json)
  end

  def region
    @region_rank ||= Result::Region.new(athlete, division, 1, athlete.region_id)
  end

  def affiliate
    if athlete.affiliate_id.to_i > 0
      @affiliate_rank ||= Result::Affiliate.new(athlete, division, fittest, fittest1)
    end
  end

  def world
    @world_rank ||= Result::World.new(athlete, division, 1, 0)
  end

  def total_athletes
    total_full_pages + last_page
  end

  def total_full_pages
    ( total_pages - 1 ) * 50
  end

  def percentage
    ( ( rank / total_athletes.to_f ) * 100 )
  end

  def total_pages
    json["totalpages"]
  end

  def last_page
    res = HTTP.get(last_url(total_pages))
    JSON.parse(res.to_s)["athletes"].size
  end

  class Affiliate < Result
    def last_url(page)
      "#{base_url}affiliate=#{athlete.affiliate_id}&page=#{page}"
    end

    def url
      "#{base_url}affiliate=#{athlete.affiliate_id}&page=#{page}"
    end

    def total_athletes
      if json["totalpages"] > 1
        super
      else
        json["athletes"].size.to_f
      end
    end

    def athlete_info
      @page = page
      @athlete_info ||= json["athletes"].find{|a| a["userid"] == athlete.id }
      if @athlete_info.present?
        @athlete_info
      else
        while @athlete_info.blank? && page <= total_pages do
          @page = @page + 1
          @json = nil
          @response = nil
          @athlete_info = json["athletes"].find{|a| a["userid"] == athlete.id }
        end
        @athlete_info
      end
    end

    def page
      @page ||= 1
    end

  end

  class World < Result
  end

  class Region < Result
  end

  class State < Result
    def state_correct?
      json["athletes"].size > 0
    end

    def fittest1
      @fittest1.upcase
    end
  end
end
