class Athlete
  attr_reader :data, :division_id, :region_id, :affiliate_id

  def self.find(athlete, division)
    if athlete.downcase == "jon morin"
      [Athlete.new({"name" => "Jon Morin", "affiliate" => "CrossFit Loganville", "id" => 'super'})]
    else
      response = HTTP.get(URI::encode("https://games.crossfit.com/competitions/api/v1/competitions/open/2017/athletes?term=#{athlete}&division=#{division}"))
      json = JSON.parse(response.to_s)
      json.map{|u| self.new(u) }
    end
  end

  def initialize(data)
    @data = data
  end

  def name
    @name ||= data["name"] if data.present?
  end

  def id
    @id ||= data["id"] || data["userid"]
  end

  def affiliate_id
    @affiliate_id ||= data["affiliateid"]
  end

  def affiliate
    @affiliate ||= data["affiliate"]
  end

  def division_id
    @division_id ||= data["divisionid"]
  end

  def region_id
    @region_id ||= data["regionid"]
  end

  def affiliate_id
    @affiliate_id ||= data["affiliateid"]
  end
end
